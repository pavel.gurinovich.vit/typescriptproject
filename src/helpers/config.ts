
const secret: string = "SECRET_KEY"
const mongoUri: string = "mongodb://localhost:27017/teamDB"


export {
    secret, 
    mongoUri
}
