import {Schema, model} from 'mongoose'

interface ICart {
    products: Array<Schema.Types.ObjectId>
    user: Schema.Types.ObjectId
}

const cartSchema = new Schema<ICart>({
    products: [{
        type: Schema.Types.ObjectId,
        ref: "Product",
    }],
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
    },
});
  

const Cart = model<ICart>('Cart', cartSchema)
export {Cart, ICart}