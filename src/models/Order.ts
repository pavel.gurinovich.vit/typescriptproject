import {Schema, model} from 'mongoose'

interface IOrder {
    cart: Array<Schema.Types.ObjectId>
    user: Schema.Types.ObjectId
}

const orderSchema = new Schema<IOrder>({
    cart: [{
        type: Schema.Types.ObjectId,
        ref: 'Cart',
    }],
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
    }
})

const Order = model<IOrder>('Order', orderSchema)

export {Order, IOrder}