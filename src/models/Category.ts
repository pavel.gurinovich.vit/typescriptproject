import {Schema, model} from 'mongoose'

interface ICategory {
    name: string
    products: Array<Schema.Types.ObjectId>
}

const categorySchema = new Schema<ICategory>({
    name:{
        type: String,
        required: true,
    },
    products:[{
        type: Schema.Types.ObjectId,
        ref: 'Product'
    }]
})

const Category = model<ICategory>('Category', categorySchema)
export {Category, ICategory}