import {Schema, model} from 'mongoose'

interface IProduct {
    name: string
    price: number
    category: Schema.Types.ObjectId
}


const productSchema = new Schema<IProduct>({
    name:{
        type: String,
    },
    price:{
        type: Number,
    },
    category:{
        type: Schema.Types.ObjectId,
        ref: 'Category'
    }
});

const Product = model<IProduct>('Product', productSchema)
export {Product, IProduct}