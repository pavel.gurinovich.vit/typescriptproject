import {Schema, model} from 'mongoose'

interface IUser {
    email: string
    password: string
    name: string
    surname: string
    role:string
    order:Array<Schema.Types.ObjectId>
    cart:Schema.Types.ObjectId
}

const userSchema = new Schema<IUser>({
    email:{
        type: String,
        unique: true,
        required: true,
    },
    password:{
        type: String,
    },
    name:{
        type: String,
        required: true,
    },
    surname:{
        type: String,
        required: true,
    },
    role:{
        type: String,
        required: true,
        default: "USER"
    },
    order:[{
        type: Schema.Types.ObjectId,
        ref: 'Order'
    }],
    cart:{
        type: Schema.Types.ObjectId,
        ref: 'Cart',
    } 
})

const User = model<IUser>('User', userSchema)
export {User, IUser}
