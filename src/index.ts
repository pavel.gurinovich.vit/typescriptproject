import express from 'express'
import mongoose from 'mongoose'
import {json} from 'body-parser'
import db from './db/db'
const app = express()
const PORT = 3000
app.use(json())

mongoose.connect("mongodb://localhost:27017/teamDB", )

app.listen(PORT, ()=>{
    db
    console.log(`Sever is running on port:  ${PORT}`)
})
